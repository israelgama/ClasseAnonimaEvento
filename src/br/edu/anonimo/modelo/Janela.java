package br.edu.anonimo.modelo;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class Janela extends Frame {
	
	private Button button;
	
	public Janela() {
		super("Exemplo do ActionListener");
		setLayout(new FlowLayout());
		
		button = new Button("Clique aqui!!");
		
		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				dispose();
			}
			
		});
		
		add(button);
		setSize(400,200);
		setLocationRelativeTo(null);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		Janela jl=new Janela();
		
	}
	
}
