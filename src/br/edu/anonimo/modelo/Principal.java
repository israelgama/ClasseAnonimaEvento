package br.edu.anonimo.modelo;

public class Principal {

	public static void main(String[] args) {

		Botao botaoSair = new Botao();
		botaoSair.clicar(new IEvento() {

			@Override
			public void execute() {
				System.out.println("Um bot�o foi clicado!!");			
			}
			
		});
		
		CaixaDeTexto cxTexto = new CaixaDeTexto();
		cxTexto.clicar(new IEvento() {

			@Override
			public void execute() {
				System.out.println("O Texto foi digitado!!");
				
			}
			
		});
		
		
	}

}
