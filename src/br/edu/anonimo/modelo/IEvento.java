package br.edu.anonimo.modelo;

/**
 * Mostra a l�gica de funcionamento
 * da classe ActionListener do awt/swing
 * @author aluno
 *
 */
public interface IEvento {

	public void execute();
	
}
